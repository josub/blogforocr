import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() title: string;
  @Input() content: string;
  @Input() created_at;
  @Input() loveIts: number;


  lovePlusUn(){
      this.loveIts += 1;
  }

  loveMoinsUn(){
      this.loveIts -= 1;
  }
  constructor() {
   }

  ngOnInit() {
      return this.created_at = new Date();
  }

}
